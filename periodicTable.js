/*global $, document */
$(document).ready(function() {
	$(".learnview").hide();
	$(".answerview").hide();
	$(".hide-learning").hide();
	$(".show-learning").show();
});
	
$("#showMajorSystem").click(function() {
	$(".defaultview").toggle();
	$(".learnview").toggle();
});

$("#test").click(function() {
	$(".defaultview, .learnview").hide();
	$(".hide-playing").hide();
	$(".show-playing").show();
	
	$(".answerview").show();
});

$("#check").click(function() {
	$(".elem-box").each(function() {
		var element = $(this).find(".elemname").html();
		var	answer = $(this).find(".elemanswer").val();
		if (answer) {
			if(answer.toLowerCase().score(element.toLowerCase(), 0.6) > 0.5) {
				$(this).append('<i class="correct fa fa-check-circle fa-2x"</i>');
				if (answer.toLowerCase() !== element.toLowerCase()) {
					$(this).append('<span class="elemtext">'+element+'</span>');
				}
			}
			else {
				$(this).append('<i class="incorrect fa fa-times-circle fa-2x"></i><p>'+element+'</p>');
			}
		}
		else {
			$(this).append('<p>'+element+'</p>');
		}
		$(".hide-finished").hide();
		$(".show-finished").show();
	});
});

$("#repeat").click(function() {
	location.reload();
});
